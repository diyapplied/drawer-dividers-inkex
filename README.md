# drawer-dividers-inkex

Drawer Dividers Inkscape Extension. Draw top-down view how you want a drawer to be divided, and this extension will render drawer divider paths you can laser cut (or I guess manually cut) and assemble.

## Requirements

Known to only work for Inkscape 1.0. Your milelage may vary...

## How To Install

Download the .inx and .py files, copy & paste them to your user extensions folder.

You can find your user extensions folder by opening Inkscape and going to Edit > Preferences... > System (left column) > User extensions (right side). 

For Windows, it'll likely be: C:\Users\your-user-name\AppData\Roaming\inkscape\extensions

## Tutorials

Check out the tutorial folder.