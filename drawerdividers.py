#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2020 Nathan Armentrout, diyapplied@gmail.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import inkex
from inkex.paths import Line, Path
import pathlib

class Divider():
    height = 0.0
    thickness = 0.0
    chamfer = 0.0
    def __init__(self, len):
        self.length = len
        self.notches = []

    def __str__(self):
        return 'Divider ' + str(self.length) + ' units long with notches at: ' + str(self.notches)

    @property
    def path_string(self):
        has_front_notch = False
        has_back_notch = False
        half_thickness = self.thickness / 2
        half_height = self.height / 2
        self.notches.sort()
        result = 'M {:.4f} {:.4f} '.format(-1.0 * half_thickness, half_height) if self.notches[0] <= self.thickness else 'M 0.0 0.0 '
        if self.chamfer > 0.0:
            for n in self.notches:
                if n <= self.thickness:
                    # T notch
                    has_front_notch = True 
                    result += 'H {:.4f} '.format(half_thickness)
                    result += 'V {:.4f} '.format(self.chamfer)
                    result += 'L {:.4f} {:.4f} '.format(half_thickness + self.chamfer, 0.0)
                elif n >= self.length - self.thickness:
                    # T notch
                    has_back_notch = True
                    result += 'H {:.4f} '.format(n - half_thickness - self.chamfer)
                    result += 'L {:.4f} {:.4f} '.format(n - half_thickness, self.chamfer)
                    result += 'V {:.4f} '.format(half_height)
                    result += 'H {:.4f} '.format(self.length + half_thickness)
                else:
                    # Interior notch
                    result += 'H {:.4f} '.format(n - half_thickness - self.chamfer)
                    result += 'L {:.4f} {:.4f} '.format(n - half_thickness, self.chamfer)
                    result += 'V {:.4f} '.format(half_height)
                    result += 'H {:.4f} '.format(n + half_thickness)
                    result += 'V {:.4f} '.format(self.chamfer)
                    result += 'L {:.4f} {:.4f} '.format(n + half_thickness + self.chamfer, 0.0)
        else:
            for n in self.notches:
                if n <= self.thickness:
                    # T notch
                    has_front_notch = True
                    result += 'H {:.4f} '.format(half_thickness)
                    result += 'V {:.4f} '.format(0.0)
                elif n >= self.length - self.thickness:
                    # T notch
                    has_back_notch = True
                    result += 'H {:.4f} '.format(n - half_thickness)
                    result += 'V {:.4f} '.format(half_height)
                    result += 'H {:.4f} '.format(self.length + half_thickness)
                else:
                    # Interior notch
                    result += 'H {:.4f} '.format(n - half_thickness)
                    result += 'V {:.4f} '.format(half_height)
                    result += 'H {:.4f} '.format(n + half_thickness)
                    result += 'V 0.0 '
        if not has_back_notch:
            result += 'H {:.4f} '.format(self.length)
        result += 'V {:.4f} '.format(self.height)
        result += 'H {:.4f} Z '.format(0.0 if not has_front_notch else -1.0 * half_thickness)
        return result

def lines_intersect(hp, vp):
    #assume absolute
    hx1 = hp[0].args[0]
    hy1 = hp[0].args[1]
    hx2 = hp[1].args[0]

    vx1 = vp[0].args[0]
    vy1 = vp[0].args[1]
    vy2 = vp[1].args[0]

    return (vy1 <= hy1 <= vy2 or vy2 <= hy1 <= vy1) and (hx1 <= vx1 <= hx2 or hx2 <= vx1 <= hx1)

class DrawerDivider(inkex.EffectExtension):
    # Constants
    Y_OFFSET_BUFFER = 10.0

    def add_arguments(self, pars):
        pars.add_argument("--depth", type=float, default=25.0,
                          help="Height/depth of the drawer the dividers are")
        pars.add_argument("--thickness", type=float, default=5.0,
                          help="Material thickness to be laser cut")
        pars.add_argument("--units", help="Inches or mm")
        pars.add_argument("--chamfer", type=float, default=0.0, help="Chamfer run")
        pars.add_argument("--incl_bounding_box", type=inkex.Boolean, default=False, help="Include Bounding Box")

    def effect(self):
        # Assess the SVG
        # Classify path as horz or vert, ignore others
        bounding_box_found = False
        box_min_x = float('inf') if self.options.incl_bounding_box else float('-inf')
        box_max_x = float('-inf') if self.options.incl_bounding_box else float('inf')
        box_min_y = float('inf') if self.options.incl_bounding_box else float('-inf')
        box_max_y = float('-inf') if self.options.incl_bounding_box else float('inf')
        horz_paths = []
        vert_paths = []
        render_y_offset = 0.0
        for elem in self.svg.get_selected_or_all(inkex.PathElement):
            path = elem.path.to_absolute()
            if len(path) == 5:
                # This is probably a bounding box
                if self.options.incl_bounding_box:
                    if not bounding_box_found:
                        bounding_box_found = True
                        for cp in path.control_points:
                            box_min_x = min(box_min_x, cp.x)
                            box_max_x = max(box_max_x, cp.x)
                            box_min_y = min(box_min_y, cp.y)
                            box_max_y = max(box_max_y, cp.y)
                    else:
                        raise Exception("Multiple possible bounding boxes found. Please ensure drawing only has 1 box.")
            elif len(path) == 2:
                # This is probably a divider
                if path[1].letter == 'H':
                    horz_paths.append(path)
                    for ep in path.end_points:
                        if ep.y > render_y_offset:
                            render_y_offset = ep.y
                elif path[1].letter == 'V':
                    vert_paths.append(path)
                    for ep in path.end_points:
                        if ep.y > render_y_offset:
                            render_y_offset = ep.y
                else:
                    #warning: can only process horizontal or vertical paths
                    pass
            else:
                # warning: cannot process complex paths or points
                pass
        render_y_offset += self.Y_OFFSET_BUFFER
        
        # Standardize & Filter Inputs
        specd_height_mm = self.options.depth * (1.0 if self.options.units == 'mm' else 25.4)
        specd_thickness_mm = self.options.thickness * (1.0 if self.options.units == 'mm' else 25.4)
        specd_chamfer = self.options.chamfer * (1.0 if self.options.units == 'mm' else 25.4)

        max_chamfer_mm = specd_height_mm / 4
        adj_chamfer_mm = specd_chamfer if specd_chamfer < max_chamfer_mm else max_chamfer_mm 

        # Create Divider objects used to generate new divider paths
        Divider.height = specd_height_mm
        Divider.thickness = specd_thickness_mm
        Divider.chamfer = adj_chamfer_mm
        dividers = []
        for hp in horz_paths:
            h_min_x = max(min(hp[0].args[0], hp[1].args[0]), box_min_x)
            h_max_x = min(max(hp[0].args[0], hp[1].args[0]), box_max_x)
            d = Divider(h_max_x - h_min_x)
            for vp in vert_paths:
                if lines_intersect(hp, vp):
                    vx = vp[0].args[0]
                    if h_min_x <= vx <= h_max_x:
                        d.notches.append(vx - h_min_x)
                    else:
                        with open(r'C:\Users\Nate\Desktop\debug.txt', 'a') as f:
                            f.write('H {:.4f} {:.4f} {:.4f} \n'.format(h_min_x, vx, h_max_x))
            dividers.append(d)
        
        for vp in vert_paths:
            v_min_y = max(min(vp[0].args[1], vp[1].args[0]), box_min_y)
            v_max_y = min(max(vp[0].args[1], vp[1].args[0]), box_max_y)
            d = Divider(v_max_y - v_min_y)
            for hp in horz_paths:
                if lines_intersect(hp, vp):
                    hy = hp[0].args[1]
                    if v_min_y <= hy <= v_max_y:
                        d.notches.append(hy - v_min_y)
                    else:
                        with open(r'C:\Users\Nate\Desktop\debug.txt', 'a') as f:
                            f.write('V {:.4f} {:.4f} {:.4f} \n'.format(v_min_y, hy, v_max_y))
            dividers.append(d)

        # Render Dividers
        line_style = {
            'stroke': '#FF0000', 
            'fill': 'none',
            'stroke-width': str(self.svg.unittouu('{:0.2f}mm'.format(Divider.thickness * 0.1)))
        }
        for d in dividers:
            index = dividers.index(d)
            pe = self.svg.get_current_layer().add(inkex.PathElement(id='Divider ' + str(index)))
            pe.set_path(Path(d.path_string).translate(0, render_y_offset + (index * (d.height + d.thickness))).rotate(180))
            pe.style = line_style
                
if __name__ == '__main__':
    DrawerDivider().run()
